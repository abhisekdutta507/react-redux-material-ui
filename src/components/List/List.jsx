import React, { Component } from 'react';
import './List.css';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// https://material-ui.com/components/snackbars/
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import SnackbarElement from '../Snackbar/Snackbar.jsx';

import * as _counter from '../../stores/actions/counter.action';
import * as _snackbar from '../../stores/actions/snackbar.action';

class List extends Component {

  constructor(props) {
    super(props);

    this.state = {
      snackbar: {
        open: false,
        variant: 'info',
        message: ''
      }
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      snackbar: {
        open: true,
        variant: 'error',
        message: 'This is an error message!'
      }
    });

    this.props._showSnackbar();
  }

  render() {
    return (
      <div className={'list'}>
        <p className={'para'}> Item {this.props.id} </p>
        <IconButton color="secondary" className={'close'} aria-label="delete" onClick={this.handleClick}>
          <DeleteIcon />
        </IconButton>

        <SnackbarElement variant={this.state.snackbar.variant} message={this.state.snackbar.message}/>
      </div>
    );
  }

};

const mapStateToProps = (state, props) => {
  return state;
};

const mapActionToProps = (dispatch, props) => {
  return bindActionCreators({
    _incrementCounter: _counter.incrementCounter,
    _decrementCounter: _counter.decrementCounter,
    _updateCounter: _counter.updateCounter,

    _toggleSnackbar: _snackbar.toggleSnackbar,
    _showSnackbar: _snackbar.showSnackbar,
    _hideSnackbar: _snackbar.hideSnackbar,
  }, dispatch);
};

export default connect(mapStateToProps, mapActionToProps)(List);