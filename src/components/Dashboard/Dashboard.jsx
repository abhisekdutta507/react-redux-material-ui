import React, { Component } from 'react';
import './Dashboard.css';

import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
// import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import Notifications from '@material-ui/icons/Notifications';
import NotificationsActive from '@material-ui/icons/NotificationsActive';

import * as _counter from '../../stores/actions/counter.action';
import * as _snackbar from '../../stores/actions/snackbar.action';
import * as _app from '../../stores/actions/app.action';

import Http from '../../services/Http';
import AppList from '../List/List';

const http = new Http();

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: {},
      updateMessage: ''
    };

    let su = localStorage.getItem('user');
    let pu = su && JSON.parse(su);
    if (!pu) {
      this.props.history.push('login');
    }
  }

  _updateApp() {
    this.props._updateApp({type: 'user', payload: {fname: 'Arindam', lname: 'Dutta'}});

    let tmp = window.ReactNativeWebView && window.ReactNativeWebView.postMessage(JSON.stringify({type: 'login', response: localStorage.getItem('user')}));
  }

  _showFname() {
    let tmp = window.ReactNativeWebView && window.ReactNativeWebView.postMessage(JSON.stringify({type: 'show'}));
  }

  _removeFname() {
    let tmp = window.ReactNativeWebView && window.ReactNativeWebView.postMessage(JSON.stringify({type: 'logout', response: JSON.stringify(this.props.app.user)}));
    
    // this.props.history.goBack();
    localStorage.removeItem('user');
    this.props.history.push('login');
  }

  async _sendPushNotification(u) {
    await http._sendPushNotification({
      topic: 'Sale is live',
      title: '$GOOG up 1.43% on the day',
      message: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
      to: [u.email]
    });
  }

  async _sendPushNotificationToAll() {
    await http._sendPushNotification({
      topic: 'Sale is live',
      title: '$GOOG up 1.43% on the day',
      message: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
      to: this.props.app.users.map(_ => _.email)
    });
  }

  UNSAFE_componentWillMount() {
    
  }

  async componentDidMount() {
    let r = await http._loggedInUsers();
    this.props._updateApp({type: 'users', payload: (r && r.body && r.body.data) ? r.body.data : ''});
  }

  componentWillUnmount() {
    
  }

  getFullname(u) {
    let su = localStorage.getItem('user');
    let pu = su && JSON.parse(su);
    if (u || pu) {
      return <p>Hello, {`${pu.fname || u.fname} ${pu.lname || u.lname}`}!</p>;
    }
    return <p>Hello, Stranger!</p>;
  }

  render() {
    return (
      <div className={'dashboard'}>
        <div className={'row'}>
          <div className={'col'}>
            <h3> Welcome to our Dashboard </h3>
          </div>
        </div>
        <div className={'row'}>
          <div className={'col'}>
            {this.getFullname(this.props.app.user)}
          </div>
        </div>

        <div className={'d-none'}>
          <div>
            <ul>
              <li> <Link to={'/dashboard'} className={'nav-link'}> Dashboard </Link> </li>
              <li> <Link to={'/login'} className={'nav-link'}> Login  </Link> </li>
            </ul>
          </div>

          <div>
            <ul>
              <li> <AppList id={1} /> </li>
              <li> <AppList id={2} /> </li>
              <li> <AppList id={3} /> </li>
            </ul>
          </div>

          <div className={'update'}>
            <button onClick={this._updateApp.bind(this)}> Update </button>
          </div>
          <div className={'showFname'}>
            <button onClick={this._showFname.bind(this)}> Show </button>
          </div>
          <div className={'removeFname'}>
            <button onClick={this._removeFname.bind(this)}> Remove </button>
          </div>
        </div>

        <div className={'row'}>
          <div className={'col w-100'}>
            {
              !this.props.app.users && 'List will be displayed here...'
            }
            <List className={'users'}>
              {this.props.app.users && this.props.app.users.map(u => {
                return (
                  <ListItem key={u._id}>
                    <ListItemText id={u._id} primary={`${u.email}`} />
                    <ListItemSecondaryAction>
                      <IconButton edge="end" aria-label="comments" onClick={() => {this._sendPushNotification(u)}}>
                        <NotificationsActive style={{ fontSize: 30, color: '#00897b' }} />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                );
              })}
            </List>
          </div>
        </div>

        <div className={'row center'}>
          {
            this.props.app.users && (
              <div className={'col'}>
                {/* <button className={'input-button'} onClick={this._sendPushNotificationToAll.bind(this)}>  </button> */}
                <Button onClick={this._sendPushNotificationToAll.bind(this)} variant="contained" size="medium" style={{color: '#fff', backgroundColor: '#00897b'}}>Send Push To All</Button>
              </div>
            )
          }
          <div className={'col'}>
            {/* <button className={'input-button'} onClick={this._removeFname.bind(this)}> Logout </button> */}
            <Button onClick={this._removeFname.bind(this)} variant="contained" size="medium" style={{color: '#fff', backgroundColor: '#00897b'}}>Logout</Button>
          </div>
        </div>

        <div>
          <p> {this.state.updateMessage} </p>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    ...state
  };
};

const mapActionToProps = (dispatch, props) => {
  return bindActionCreators({
    _incrementCounter: _counter.incrementCounter,
    _decrementCounter: _counter.decrementCounter,
    _updateCounter: _counter.updateCounter,

    _toggleSnackbar: _snackbar.toggleSnackbar,
    _showSnackbar: _snackbar.showSnackbar,
    _hideSnackbar: _snackbar.hideSnackbar,

    _updateApp: _app.updateApp
  }, dispatch);
};

export default connect(mapStateToProps, mapActionToProps)(Dashboard);