import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Login.css';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';

import * as _counter from '../../stores/actions/counter.action';
import * as _app from '../../stores/actions/app.action';

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: {
        fname: '',
        lname: '',
        email: '',
        password: '',
      }
    };

    let su = localStorage.getItem('user');
    let pu = su && JSON.parse(su);
    if (pu) {
      this.props.history.push('dashboard');
    }
  }

  _incrementCounter() {
    this.props._incrementCounter(1);
  }

  _decrementCounter() {
    this.props._decrementCounter(1);
  }

  UNSAFE_componentWillMount() {
    
  }

  componentDidMount() {}

  componentWillUnmount() {}
  
  getFullname(u) {
    if (u) {
      return <p>Hello, {`${u.fname} ${u.lname}`}!</p>;
    }
    return <p>Hello, Stranger!</p>;
  }

  loginFunction(e) {
    e.preventDefault();

    let form = document.querySelector('form[name="login-form"]');

    if(form.fname.value && form.lname.value && form.email.value && form.password.value) {
      let u = {
        fname: form.fname.value,
        lname: form.lname.value,
        email: form.email.value,
        password: form.password.value,
      };

      let tmp = window.ReactNativeWebView && window.ReactNativeWebView.postMessage(JSON.stringify({type: 'login', response: JSON.stringify(u)}));
      localStorage.setItem('user', JSON.stringify(u));
      this.props._updateApp({type: 'user', payload: u});
      this.setState({user: u});

      this.props.history.push('dashboard');
    }

    return false;
  }

  render() {

    return (
      <div className="landing">
        <div className={'tutorial d-none'}>

          <div> <h3> React js tutorial </h3> </div>
          <div> {this.getFullname(this.state.user)} </div>

          <ul>
            <li> <Link to={'/login'} className="nav-link"> Login </Link> </li>
            <li> <Link to={'/dashboard'} className="nav-link"> Dashboard </Link> </li>
          </ul>

          <div className={'redux'}>
            <div> <h3> React Redux Section </h3> </div>

            <p> Counter is {this.props.counter} </p>

            <div className={'button m-v'}>

              <Button variant="contained" color="primary" className={'add-button'} onClick={this._incrementCounter.bind(this)}>
                +
              </Button>

            </div>
            <div className={'button m-v'}>

              <Button variant="contained" color="primary" className={'subtract-button'} onClick={this._decrementCounter.bind(this)}>
                -
              </Button>
            </div>
          </div>

        </div>

        <div className={'login m-t-100'}>
          <form name={'login-form'} onSubmit={this.loginFunction.bind(this)}>
            <div className={'row'}>
              <div className={'col w-100'}>
                <h3 className={'heading'}> Login here </h3>
              </div>
            </div>
            <div className={'row'}>
              <div className={'col w-100'}>
                <input name={'fname'} type="text" className={'w-100 input-text'} placeholder={'fname'} autoComplete={'off'} />
              </div>
            </div>
            <div className={'row'}>
              <div className={'col w-100'}>
                <input name={'lname'} type="text" className={'w-100 input-text'} placeholder={'lname'} autoComplete={'off'} />
              </div>
            </div>
            <div className={'row'}>
              <div className={'col w-100'}>
                <input name={'email'} defaultValue={''} type={'email'} className={'w-100 input-text'} placeholder={'email'} autoComplete={'off'} />
              </div>
            </div>
            <div className={'row d-none'}>
              <div className={'col w-100'}>
                <input name={'password'} defaultValue={'123456'} type="password" className={'w-100 input-text'} placeholder={'password'} autoComplete={'off'} />
              </div>
            </div>
            <div className={'row align-right'}>
              <div className={'col'}>
                <button type="reset" className={'input-button'}> Reset </button>
              </div>
              <div className={'col'}>
                <button type="submit" className={'input-button'}> Submit </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return state;
};

const mapActionToProps = (dispatch, props) => {
  return bindActionCreators({
    _incrementCounter: _counter.incrementCounter,
    _decrementCounter: _counter.decrementCounter,
    _updateCounter: _counter.updateCounter,

    _updateApp: _app.updateApp
  }, dispatch);
};

export default connect(mapStateToProps, mapActionToProps)(Login);