export const toggleSnackbar = () => {
  return {
    type: 'TOGGLE'
  }
}

export const showSnackbar = () => {
  return {
    type: 'SHOW'
  }
}

export const hideSnackbar = () => {
  return {
    type: 'HIDE'
  }
}