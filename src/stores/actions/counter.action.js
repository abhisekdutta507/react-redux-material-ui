export const incrementCounter = (n = 1) => {
  return {
    type: 'INCREMENT',
    payload: n
  }
}

export const decrementCounter = (n = 1) => {
  return {
    type: 'DECREMENT',
    payload: n
  }
}

export const updateCounter = (n) => {
  return {
    type: 'UPDATE',
    payload: n
  }
}