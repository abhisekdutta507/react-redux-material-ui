const app = (state = {}, action) => {
  state[action.type] = action.payload;
  return {
    ...state
  }
 
}

export default app;