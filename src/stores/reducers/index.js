import app from './app.reducer';
import counter from './counter.reducer';
import snackbar from './snackbar.reducer';

import { combineReducers } from 'redux';

const Reducers = combineReducers({
  app: app,
  counter: counter,
  snackbar: snackbar
});

export default Reducers;