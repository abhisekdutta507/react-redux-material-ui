import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
// import logo from './logo.svg';
import './App.css';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Login from './components/Login/Login.jsx';
import Dashboard from './components/Dashboard/Dashboard.jsx';

import * as _counter from './stores/actions/counter.action';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {

    return (
      <div className={'app'}>
        <Router >

          {/* Visit https://reacttraining.com/react-router/web/example/custom-link for activatedRouterLink concept */}

          <Switch>
            <Redirect exact from='/' to='/dashboard'/>
            <Route path={'/login'} component={Login} />
            <Route path={'/dashboard'} component={Dashboard} />
          </Switch>

        </Router>

      </div>
    );
  }
}

// https://stackblitz.com/edit/react-redux-simple-boilerplate

const mapStateToProps = (state, props) => {
  return state;
};

const mapActionToProps = (dispatch, props) => {
  return bindActionCreators({
    _incrementCounter: _counter.incrementCounter,
    _decrementCounter: _counter.decrementCounter,
    _updateCounter: _counter.updateCounter,
  }, dispatch);
};

export default connect(mapStateToProps, mapActionToProps)(App);
