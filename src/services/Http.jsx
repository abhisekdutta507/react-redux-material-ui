import {Component} from 'react';
import axios from 'axios';

let that;

export default class Http extends Component {
  constructor(props) {
    super(props);
    that = this;
    this.state = {};
  }

  _responseObject = (r) => Object.assign(
    {
      timeout: r.timeout,
      status: r.status,
      statusText: r.statusText,
      readyState: r.readyState,
      responseURL: r.responseURL,
      responseText: r.responseText,
      responseJSON: JSON.parse(r.response)
    }
  )

  _testHttpRequest() {
    return new Promise((next, error) => {
      let xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
          next(that._responseObject(this));
        }
      };

      xhttp.open("GET", "https://nodeapis101.herokuapp.com/api/v1/test", true);
      xhttp.send();
    });
  }

  _logOutUser(email) {
    return new Promise((next, error) => {
      axios({
        url: `https://nodeapis101.herokuapp.com/api/v1/forthlife/user/${email}`,
        method: 'DELETE'
      }).then(r => {r.body = r.data; delete r.data; next(r);}).catch(e => next(e));
    });
  }

  _loggedInUsers() {
    return new Promise((next, error) => {
      axios({
        url: 'https://nodeapis101.herokuapp.com/api/v1/forthlife/users',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(r => {r.body = r.data; delete r.data; next(r);}).catch(e => next(e));
    });
  }

  _sendPushNotification(message) {
    return new Promise((next, error) => {
      axios({
        url: 'https://nodeapis101.herokuapp.com/api/v1/forthlife/sendpush',
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        data: message
      }).then(r => {r.body = r.data; delete r.data; next(r);}).catch(e => next(e));
    });
  }
}